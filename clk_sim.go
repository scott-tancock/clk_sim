package main

import (
	"fmt"
	"os"
	"encoding/csv"
	"strconv"
	"flag"
	//"math"
)

func close(f *os.File) {
	f.Sync()
	f.Close()
}

func main() {
	var outfile string
	var logfile string
	var histfile string
	var nbins int64
	var periodA int64
	var periodB int64
	var endTime int64

	flag.StringVar( &outfile, "o", "clock-diff.csv",            "Clock Differences")
	flag.StringVar(&histfile, "h", "clock-hist.csv", "Clock relationship histogram")
	flag.StringVar( &logfile, "l",               "",                     "Log file")
	flag.Int64Var(    &nbins, "n",              100,     "Number of histogram bins")
	flag.Int64Var(  &periodA, "A",            10000,         "Clock A period in ps")
	flag.Int64Var(  &periodB, "B",         10000099,         "Clock B period in ps")
	flag.Int64Var(  &endTime, "e",     100000000000,    "Simulation end time in ps")

	flag.Parse()

	flog := os.Stdout
	ferr := os.Stderr
	if logfile != "" {
		f, err := os.Create(logfile)
		if err != nil {
			fmt.Fprintf(ferr, "Error: Could not create log file %v: %v\n", logfile, err)
			os.Exit(1)
		}
		flog = f
		defer close(flog)
	}
	
	fout, err := os.Create(outfile)
	if err != nil {
		fmt.Fprintf(ferr, "Error: Could not create log file %v: %v\n", outfile, err)
		os.Exit(2)
	}
	wo := csv.NewWriter(fout)
	defer wo.Flush()

	// Main loop
	var timeA int64 = 0
	var timeB int64 = 0
	var diffs [][]int64 = make([][]int64, 3)
	diffs[0] = make([]int64, endTime / periodB)
	diffs[1] = make([]int64, endTime / periodB)
	diffs[2] = make([]int64, endTime / periodB)
	var idx int64 = 0
	var max_diff int64 = 0
	for timeA = int64(0); timeA < endTime; timeA += periodA {
		if (timeB + periodB) > timeA {
			continue
		} else {
			for timeB += periodB; timeB < timeA; timeB += periodB {
				diffs[0][idx] = timeA
				diffs[1][idx] = timeB
				diffs[2][idx] = timeA - timeB
				idx += 1
				if timeA-timeB > max_diff {
					max_diff = timeA-timeB
				}
			}
		}
	}
	
	var diff_str [][]string = make([][]string, idx+1)
	diff_str[0] = make([]string, 3)
	diff_str[0][0] = "timeA"
	diff_str[0][1] = "timeB"
	diff_str[0][2] = "difference"
	for i := int64(0); i < idx; i++ {
		diff_str[i+1] = make([]string, 3)
		diff_str[i+1][0] = strconv.FormatInt(diffs[0][i], 10)
		diff_str[i+1][1] = strconv.FormatInt(diffs[1][i], 10)
		diff_str[i+1][2] = strconv.FormatInt(diffs[2][i], 10)
	} 
	err = wo.WriteAll(diff_str)
	if err != nil {
		fmt.Fprintf(ferr, "Error: could not write csv to file %v: %v\n", outfile, err)
		os.Exit(3)
	}

	fhist, err := os.Create(histfile)
	if err != nil {
		fmt.Fprintf(ferr, "Error: could not create file %v: %v\n", histfile, err)
		os.Exit(3)
	}
	defer close(fhist)
	wh := csv.NewWriter(fhist)
	defer wh.Flush()

	var bin_meds []int64 = make([]int64, nbins)
	var bin_lims []int64 = make([]int64, nbins+1)
	var hist []int64 = make([]int64, nbins)
	bin_lims[0] = 0
	for i := int64(0); i < nbins; i += 1 {
		hist[i] = 0
		bin_lims[i+1] = ((i+1) * max_diff) / nbins
		bin_meds[i] = (bin_lims[i] + bin_lims[i+1]) / 2
	}

	for i := int64(0); i < idx; i += 1 {
		j := 0
		for diffs[2][i] > bin_lims[j+1] {
			j += 1
		}
		hist[j] += 1
	}

	var hist_string [][]string = make([][]string, nbins)
	for i := int64(0); i < nbins; i += 1 {
		hist_string[i] = make([]string, 2)
		hist_string[i][0] = strconv.FormatInt(bin_meds[i], 10)
		hist_string[i][1] = strconv.FormatInt(hist[i], 10)
	}

	err = wh.WriteAll(hist_string)
	if err != nil {
		fmt.Fprintf(ferr, "Error: could not write csv to file %v: %v\n", histfile, err)
		os.Exit(4)
	}
	
}
